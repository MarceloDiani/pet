<?php 

class LeagueTable
{
	public function __construct($players)
    {
		$this->standings = array();
		foreach($players as $index => $p)
        {
			$this->standings[$p] = array
            (
                'index' => $index,
                'games_played' => 0, 
                'score' => 0
            );
        }
	}
		
	public function recordResult($player, $score)
    {
		$this->standings[$player]['games_played']++;
		$this->standings[$player]['score'] += $score;
	}




	public function playerRank($rank)
    {

    	$players_order = array();
    	foreach($this->standings as $player => $standing)
    	{
    		$players_order[] = $player;
    	}
		for($order = 0; $order < count($players_order); $order ++)
    	{
    		for($i = $order+1; $i < count($players_order); $i++)
    		{
				$player_name = $players_order[$order];
				
				$change = false; 
    			if($this->standings[$player_name]['score'] < $this->standings[$players_order[$i]]['score'])
    			{
					$change = true;	
				}
				else if($this->standings[$player_name]['score'] == $this->standings[$players_order[$i]]['score'])
				{
					if($this->standings[$player_name]['games_played'] > $this->standings[$players_order[$i]]['games_played'])
					{
						$change = true;
					}
					else if($this->standings[$player_name]['games_played'] == $this->standings[$players_order[$i]]['games_played'])
					{
						
						if($this->standings[$player_name]['index'] > $this->standings[$players_order[$i]]['index'])
						{
						
							$change = true;
						}
					}
				}
				if($change == true)
				{
					$aux = $player_name;
					$players_order[$order] = $players_order[$i];
					$players_order[$i] = $aux;
				}
    		}
    	}

        return $players_order[$rank -1];
	}
}
      
$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);