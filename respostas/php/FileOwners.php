<?php


class FileOwners
{
    public static function groupByOwners($files)
    {
		$author_array = array();
		foreach($files as $file => $owner){
			echo "${file} => {$owner} ";
			if(isset($author_array[$owner])){
				$data = $author_array[$owner];
			}else{
				$data = array();
			}
			$data[] = $file;
			$author_array[$owner] = $data;
		}
        return $author_array;
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",

);
var_dump(FileOwners::groupByOwners($files));