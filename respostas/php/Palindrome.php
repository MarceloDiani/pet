<?php


class Palindrome
{
    public static function isPalindrome($word)
    {
		$word = strtolower($word);
		$len = strlen($word);
		for ($i = 0; $i <= $len/2; $i++) {
  		  if($word[$i] != $word[$len-1-$i]){
			return false;
		}
		}
		return true;
    }
}

echo Palindrome::isPalindrome('Deleveled');